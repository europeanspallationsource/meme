// DOM XML Parser
javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

// Hashtable
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Set;


main()
{
// Initialize test archive service
RPCServer server = new RPCServer();

// Get service name from property if given.
		String server_name = System.getProperty( "SERVER_NAME",SERVER_NAME_DEFAULT );

// Create an RPC server
RPCServer server = new RPCServer();

// Initialize a db connection for the server							  
RdbServiceConnection rdbConnection =	
     new RdbServiceConnection(server_name);

rpcchannelsxml = "rdb.xdb"

// XML open file best practice
File fXmlFile = new File(rpcchannelsxml);
DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
Document doc = dBuilder.parse(fXmlFile);
doc.getDocumentElement().normalize();

// Make hashtable of rpc channels, keyed by channel name. value is the DOM Element
// describing the channel, most importantly in this case the sqlStatement.
Hashtable rpcchannels_ht = new Hashtable();

 
// Read through xml file, for each PV, register the service implementation on this server
// and build the hashtable of channels
System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
NodeList nList = doc.getElementsByTagName("channel");
for (int temp = 0; temp < nList.getLength(); temp++)
{
	Node nNode = nList.item(temp);
	System.out.println("\nAdding Channel :" + nNode.getNodeName());
	if (nNode.getNodeType() == Node.ELEMENT_NODE)
	{
   	    Element element = (Element) nNode;
	    String channelname =
	    	  element.getElementsByTagName("name").item(0).getTextContent());
            String sqlStatement = getString("sqlStatement", element);
            rpcchannels_ht.put(channelName, sqlStatement);
        }
}
server.registerService("rdb", new RdbServiceImpl(rdbConnection, )););

protected String getString(String tagName, Element element) {
    NodeList list = element.getElementsByTagName(tagName);
    if (list != null && list.getLength() > 0) {
        NodeList subList = list.item(0).getChildNodes();

        if (subList != null && subList.getLength() > 0) {
            return subList.item(0).getNodeValue();
        }
    }

    return null;
}


In RdbService.java
Every instance of the implementationof a pv carries its own sqlStatment

		// The pvAccess connection delegate for the RDB service.
		private final RdbServiceConnection connection = null;
	        private final String sqlStatement = null;

		RdbServiceImpl(RdbServiceConnection connection, String sqlStatement)
		{
			this.connection = connection;
			this.sqlStatement = sqlStatement;
		}


